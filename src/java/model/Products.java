/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model;

/**
 *
 * @author Admin
 */
public class Products {
    private int pId;
    private String pName;
    private String imgPath;
    private String description;
    private int status;
    private int cateId;
    private String kichCo;
    private String tieuChuan;
    private String beMat;
    private String tietDien;
    private String trongLuong;
    private String trongLuongCuon;
    private String duongKinhCuon;
    private String ungDung;
    private String doDay;
    private String macThep;
    private String doMaKem;
    private String dungSai;
    private String cuongDoCang;
    private String doDanDai;
    private String tiLeCarbon;
    private String kichThuoc;
    private String duongKinh;

    public Products() {
    }

    public Products(int pId, String pName, String imgPath, String description, int status, int cateId, String kichCo, String tieuChuan, String beMat, String tietDien, String trongLuong, String trongLuongCuon, String duongKinhCuon, String ungDung, String doDay, String macThep, String doMaKem, String dungSai, String cuongDoCang, String doDanDai, String tiLeCarbon, String kichThuoc, String duongKinh) {
        this.pId = pId;
        this.pName = pName;
        this.imgPath = imgPath;
        this.description = description;
        this.status = status;
        this.cateId = cateId;
        this.kichCo = kichCo;
        this.tieuChuan = tieuChuan;
        this.beMat = beMat;
        this.tietDien = tietDien;
        this.trongLuong = trongLuong;
        this.trongLuongCuon = trongLuongCuon;
        this.duongKinhCuon = duongKinhCuon;
        this.ungDung = ungDung;
        this.doDay = doDay;
        this.macThep = macThep;
        this.doMaKem = doMaKem;
        this.dungSai = dungSai;
        this.cuongDoCang = cuongDoCang;
        this.doDanDai = doDanDai;
        this.tiLeCarbon = tiLeCarbon;
        this.kichThuoc = kichThuoc;
        this.duongKinh = duongKinh;
    }

    public int getpId() {
        return pId;
    }

    public void setpId(int pId) {
        this.pId = pId;
    }

    public String getpName() {
        return pName;
    }

    public void setpName(String pName) {
        this.pName = pName;
    }

    public String getImgPath() {
        return imgPath;
    }

    public void setImgPath(String imgPath) {
        this.imgPath = imgPath;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public int getCateId() {
        return cateId;
    }

    public void setCateId(int cateId) {
        this.cateId = cateId;
    }

    public String getKichCo() {
        return kichCo;
    }

    public void setKichCo(String kichCo) {
        this.kichCo = kichCo;
    }

    public String getTieuChuan() {
        return tieuChuan;
    }

    public void setTieuChuan(String tieuChuan) {
        this.tieuChuan = tieuChuan;
    }

    public String getBeMat() {
        return beMat;
    }

    public void setBeMat(String beMat) {
        this.beMat = beMat;
    }

    public String getTietDien() {
        return tietDien;
    }

    public void setTietDien(String tietDien) {
        this.tietDien = tietDien;
    }

    public String getTrongLuong() {
        return trongLuong;
    }

    public void setTrongLuong(String trongLuong) {
        this.trongLuong = trongLuong;
    }

    public String getTrongLuongCuon() {
        return trongLuongCuon;
    }

    public void setTrongLuongCuon(String trongLuongCuon) {
        this.trongLuongCuon = trongLuongCuon;
    }

    public String getDuongKinhCuon() {
        return duongKinhCuon;
    }

    public void setDuongKinhCuon(String duongKinhCuon) {
        this.duongKinhCuon = duongKinhCuon;
    }

    public String getUngDung() {
        return ungDung;
    }

    public void setUngDung(String ungDung) {
        this.ungDung = ungDung;
    }

    public String getDoDay() {
        return doDay;
    }

    public void setDoDay(String doDay) {
        this.doDay = doDay;
    }

    public String getMacThep() {
        return macThep;
    }

    public void setMacThep(String macThep) {
        this.macThep = macThep;
    }

    public String getDoMaKem() {
        return doMaKem;
    }

    public void setDoMaKem(String doMaKem) {
        this.doMaKem = doMaKem;
    }

    public String getDungSai() {
        return dungSai;
    }

    public void setDungSai(String dungSai) {
        this.dungSai = dungSai;
    }

    public String getCuongDoCang() {
        return cuongDoCang;
    }

    public void setCuongDoCang(String cuongDoCang) {
        this.cuongDoCang = cuongDoCang;
    }

    public String getDoDanDai() {
        return doDanDai;
    }

    public void setDoDanDai(String doDanDai) {
        this.doDanDai = doDanDai;
    }

    public String getTiLeCarbon() {
        return tiLeCarbon;
    }

    public void setTiLeCarbon(String tiLeCarbon) {
        this.tiLeCarbon = tiLeCarbon;
    }

    public String getKichThuoc() {
        return kichThuoc;
    }

    public void setKichThuoc(String kichThuoc) {
        this.kichThuoc = kichThuoc;
    }

    public String getDuongKinh() {
        return duongKinh;
    }

    public void setDuongKinh(String duongKinh) {
        this.duongKinh = duongKinh;
    }
    
    }
