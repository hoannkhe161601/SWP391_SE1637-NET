/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import util.DBContext;
import model.Products;
import java.sql.*;


/**
 
 * @author Admin
 */



public class ProductDAO {
    private static final String GET_ALL_PRODUCTS = "SELECT pId,  pName,  imgPath,  description,  status,\n" +
"                                 cateId,  kichCo,  tieuChuan,  beMat,  tietDien,  trongLuong,  trongLuongCuon,\n" +
"                                 duongKinhCuon,  ungDung,  doDay,  macThep,  doMaKem,  dungSai,  cuongDoCang,\n" +
"                                 doDanDai,  tiLeCarbon,  kichThuoc,  duongKinh FROM swp_gr01.products";
    private static final String GET_AN_PRODUCT = "SELECT pId,  pName,  imgPath,  description,  status,\n" +
"                                 cateId,  kichCo,  tieuChuan,  beMat,  tietDien,  trongLuong,  trongLuongCuon,\n" +
"                                 duongKinhCuon,  ungDung,  doDay,  macThep,  doMaKem,  dungSai,  cuongDoCang,\n" +
"                                 doDanDai,  tiLeCarbon,  kichThuoc,  duongKinh FROM swp_gr01.products WHERE pId = ?";
    
    public List<Products> getAllProducts() throws SQLException {
        List<Products> list = new ArrayList<>();
        Connection conn = null;
        PreparedStatement psm = null;
        ResultSet rs = null;
        try {
            conn = DBContext.getJDBCConnection();
            if (conn != null) {
                psm = conn.prepareStatement(GET_ALL_PRODUCTS);
                rs = psm.executeQuery();
                if (rs != null) {
                    while (rs.next()) {
                        int pId = rs.getInt("pId");
                        String pName = rs.getString("pName");
                        String imgPath = rs.getString("imgPath");
                        String description = rs.getString("description");
                        int status = rs.getInt("status");
                        int cateId = rs.getInt("cateID");
                        String kichCo = rs.getString("kichCo");
                        String tieuChuan = rs.getString("tieuChuan");
                        String beMat = rs.getString("beMat");
                        String tietDien = rs.getString("tietDien");
                        String trongLuong = rs.getString("trongLuong");                 
                        String trongLuongCuon = rs.getString("trongLuongCuon");
                        String duongKinhCuon = rs.getString("duongKinhCuon");
                        String ungDung = rs.getString("ungDung");
                        String doDay = rs.getString("doDay");
                        String macThep = rs.getString("macThep");
                        String doMaKem = rs.getString("doMaKem");
                        String dungSai = rs.getString("dungSai");
                        String cuongDoCang = rs.getString("cuongDoCang");
                        String doDanDai = rs.getString("doDanDai");
                        String tiLeCarbon = rs.getString("tiLeCarbon");
                        String kichThuoc = rs.getString("kichThuoc");
                        String duongKinh = rs.getString("duongKinh");
                        
                        
                        Products product = new Products(pId,  pName,  imgPath,  description,  status,
                                 cateId,  kichCo,  tieuChuan,  beMat,  tietDien,  trongLuong,  trongLuongCuon,
                                 duongKinhCuon,  ungDung,  doDay,  macThep,  doMaKem,  dungSai,  cuongDoCang,
                                 doDanDai,  tiLeCarbon,  kichThuoc,  duongKinh);  
                        list.add(product);
                    }
                }
            }
        } catch (Exception e) {
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (psm != null) {
                psm.close();
            }
            if (conn != null) {
                conn.close();
            }
        }
        return list;
    }
    
    public Products getProduct(int pid) throws SQLException {
        Products product = null;
        Connection conn = null;
        PreparedStatement psm = null;
        ResultSet rs = null;
        try {
            conn = DBContext.getJDBCConnection();
            if (conn != null) {
                psm = conn.prepareStatement(GET_AN_PRODUCT);
                psm.setInt(1, pid);
                rs = psm.executeQuery();
                if (rs.next()) {
                    int pId = rs.getInt("pId");
                        String pName = rs.getString("pName");
                        String imgPath = rs.getString("imgPath");
                        String description = rs.getString("description");
                        int status = rs.getInt("status");
                        int cateId = rs.getInt("cateID");
                        String kichCo = rs.getString("kichCo");
                        String tieuChuan = rs.getString("tieuChuan");
                        String beMat = rs.getString("beMat");
                        String tietDien = rs.getString("tietDien");
                        String trongLuong = rs.getString("trongLuong");                 
                        String trongLuongCuon = rs.getString("trongLuongCuon");
                        String duongKinhCuon = rs.getString("duongKinhCuon");
                        String ungDung = rs.getString("ungDung");
                        String doDay = rs.getString("doDay");
                        String macThep = rs.getString("macThep");
                        String doMaKem = rs.getString("doMaKem");
                        String dungSai = rs.getString("dungSai");
                        String cuongDoCang = rs.getString("cuongDoCang");
                        String doDanDai = rs.getString("doDanDai");
                        String tiLeCarbon = rs.getString("tiLeCarbon");
                        String kichThuoc = rs.getString("kichThuoc");
                        String duongKinh = rs.getString("duongKinh");
                        
                        
                        product = new Products(pId,  pName,  imgPath,  description,  status,
                                 cateId,  kichCo,  tieuChuan,  beMat,  tietDien,  trongLuong,  trongLuongCuon,
                                 duongKinhCuon,  ungDung,  doDay,  macThep,  doMaKem,  dungSai,  cuongDoCang,
                                 doDanDai,  tiLeCarbon,  kichThuoc,  duongKinh);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (psm != null) {
                psm.close();
            }
            if (conn != null) {
                conn.close();
            }
        }
        return product;
    }

        
        
        
        
}




class t1{
    
    public static void main(String[] args) throws SQLException {
       // test getAllProducts 
//        ProductDAO d = new ProductDAO();
//        for (Products s : d.getAllProducts()) {
//            System.out.println(s);
//        }

        // test getProduct
            ProductDAO d = new ProductDAO();
            System.out.println(d.getProduct(1));
            

            
            
    }
}





